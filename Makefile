.PHONY: build test
project = bucketpusher
tests = tests
formatPaths = ${project}/ tests/ *.py

conda:
	conda create -n bucketpusher python=3.7 poetry tk=8.6.9=ha441bb4_1000
	conda activate bucketpusher
	poetry install
poetry:
	pip install poetry
	poetry install
pytest:
	poetry run pytest ${tests}
black:
	poetry run black ${formatPaths}
flake8:
	poetry run flake8 ${formatPaths}
windows:
	./windows_build.sh
macos:
	./macos_build.sh
test:
	make black
	make flake8
	make pytest
clean:
	rm -rf dist *.spec
