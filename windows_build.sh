#!/bin/bash
NAME=${APPNAME-"GCS-Uploader"}
KEY=${KEY-$(openssl rand -base64 16)}
poetry export -f requirements.txt > requirements.txt
cmd="pyinstaller app.py --clean --onefile --windowed --icon=./data/icon.ico --add-data \"data;data\" --name \"$NAME\" --key=$KEY"
docker run -v "$(pwd):/src/" cdrx/pyinstaller-windows "$cmd"
