import uuid
import tempfile
import pathlib
import google.cloud.storage as gcs
from bucketpusher.uploader import Uploader
import google.api_core.exceptions as gae


def test_fallback_to_oauth_flow_when_no_credentials(mocker):
    mocker.patch("google.cloud.storage.Client", return_value=True)

    mocked = mocker.patch("bucketpusher.uploader.Uploader.run_oauth_flow")
    with tempfile.NamedTemporaryFile() as f:
        path = pathlib.Path(f.name)
        credentials_path = pathlib.Path(str(uuid.uuid4()))
        _ = Uploader(
            user_credentials_path=credentials_path,
            client_secrets_path=path,
        )
        mocked.assert_called_with(credentials_path, path, None)


def test_upload(mocker):

    mocked_client = mocker.patch.object(
        gcs,
        "Client",
        autospec=True,
    )

    up = Uploader()
    with tempfile.NamedTemporaryFile() as f:
        up.start([f.name])

        while up.active:
            True

        client = mocked_client()
        client.bucket.assert_called_with(None)
        bucket = client.bucket(None)
        blob = bucket.blob(f.name)
        blob.upload_from_filename.assert_called_with(f.name, checksum="crc32c")


def test_upload_multiple(mocker):
    """
    Bit of a hack...
    """

    class TestCallback:
        def __init__(self):
            self.files = []

        def __call__(self, status):
            if not status.complete:
                self.files.append(status.filepath)

    _ = mocker.patch.object(
        gcs,
        "Client",
        autospec=True,
    )

    up = Uploader()
    callback = TestCallback()
    with tempfile.NamedTemporaryFile() as f1, tempfile.NamedTemporaryFile() as f2:
        files = [pathlib.Path(f1.name), pathlib.Path(f2.name)]
        up.start(files, callback)

        while up.active:
            True

        [files.remove(f) for f in callback.files]
        assert not files


def test_overwrite_error(mocker):
    mocked_client = mocker.patch.object(
        gcs,
        "Client",
        autospec=True,
    )

    def upload_from_filename(f, checksum):
        raise gae.Forbidden('"message": "permission storage.objects.delete access"')

    class TestCallback:
        def __init__(self):
            self.num_skipped = 0

        def __call__(self, status):
            self.num_skipped = status.num_skipped

    up = Uploader()

    cb = TestCallback()
    client = mocked_client()
    bucket = client.bucket(None)
    with tempfile.NamedTemporaryFile() as f:
        blob = bucket.blob(f.name)
        blob.upload_from_filename = upload_from_filename
        up.start([f.name], cb)
        while up.active:
            True

        assert cb.num_skipped == 1
