import pathlib
from bucketpusher import utils
import tempfile


def test_parse_paths_single_file():
    with tempfile.NamedTemporaryFile() as f1:
        path = pathlib.Path(f1.name)
        expected = {path: pathlib.Path(path.name)}
        for k, v in utils.parse_paths(f1.name):
            assert expected[k] == v


def test_parse_paths_from_dir():
    with tempfile.TemporaryDirectory() as tmpdir:
        expected = {}
        parent = pathlib.Path(tmpdir)
        for name in ("f1.txt", "f2.txt", "f3.log"):
            f = parent / name
            f.touch()
            expected[f] = f.relative_to(parent.parent)

        for k, v in utils.parse_paths(tmpdir):
            assert expected[k] == v


def test_parse_paths_recursion():
    with tempfile.TemporaryDirectory() as tmpdir:
        expected = {}
        parent = pathlib.Path(tmpdir)
        sub_dir = parent / "sub"
        sub_dir.mkdir(exist_ok=True)
        for i, name in enumerate(("f1.txt", "f2.txt", "f3.txt")):
            if i > 1:
                f = sub_dir / name
            else:
                f = parent / name
            f.touch()
            expected[f] = f.relative_to(parent.parent)
        for k, v in utils.parse_paths(tmpdir, True):
            assert expected[k] == v


def test_path_or_partial_path_exists_file():
    assert utils.path_or_partial_path_exists(__file__)


def test_path_or_partial_path_dir():
    assert utils.path_or_partial_path_exists(pathlib.Path(__file__).parent)


def test_path_or_partial_path_glob():
    assert utils.path_or_partial_path_exists(pathlib.Path(__file__).parent / "*.ext")
