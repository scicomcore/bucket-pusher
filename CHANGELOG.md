**unreleased**
**Version 0.4.0**
- Rename package to 'bucketpusher'
- Open log file via interface
- Provide feedback during initial file indexing
- Path parsing utility is now an iterator
- Threading architecture now supports progress callback
- Extensive logging
- Decoupled stats text from progress text
- Slight modification to layout
- Package-level logging disabled: dev must configure logger
- Removed glob support
- Dir uploads are now prefixed by the name of the source dir
**Version 0.3.0**
- Support recursive dir upload
- Tweak default text for service account file entry field
- Log file excludes backtrack and diagnostics
