#!/usr/bin/env bash

NAME=${APPNAME-"GCS-Uploader"}
APPBUNDLE="$NAME.app"
KEY=${KEY-$(openssl rand -base64 16)}

poetry install
rm -rf "./dist/$NAME" "./dist/$APPBUNDLE" *.spec
poetry run pyinstaller app.py --clean --noconsole --icon=./data/icon.icns --add-data "data:data" --name "$NAME" --key="$KEY"
rm -rf "./dist/$NAME"
# You can do this via spec-file, but then --key is not used?
sed -i '' "5i\\
<key>NSHighResolutionCapable</key> \\
<string>True</string> \\
" "./dist/$APPBUNDLE/Contents/Info.plist"

# Hack to resolve issues with tcl: https://github.com/pyinstaller/pyinstaller/issues/3820
sed -i '' "s/package require -exact Tcl.*/package require -exact Tcl 8.6.9/g" "./dist/$APPBUNDLE/Contents/Resources/tcl/init.tcl"
