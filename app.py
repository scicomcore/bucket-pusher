import datetime
from bucketpusher import app, config
from bucketpusher.__version__ import __version__
from loguru import logger
from key import key

log_path = config.get_log_file_path()
logger.remove()
logger.enable("bucketpusher")
logger.add(
    log_path,
    level="DEBUG",
    backtrace=False,
    diagnose=False,
    rotation="50 MB",
)

app = app.App(
    key=key,
    title=f"Bucket Pusher - version {__version__}",
    with_authentication=False,
    with_service_account=True,
    with_bucket_id=False,
    prefix=lambda: datetime.datetime.utcnow().strftime("%d-%m-%Y-%H:%M:%S"),
    log_file_path=log_path,
    dry_run=True,
)

if __name__ == "__main__":
    app.run()
