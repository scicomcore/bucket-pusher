Warning: This project has moved to [another repository](https://bitbucket.org/scicomcore/bucketpusher) and so this repository is no longer maintained.

# Bucket Pusher: upload files to a cloud object store

This is a Python GUI application to make uploading files to a Google Cloud Storage (GCS) bucket simple for people who are unlikely to work from the command-line, i.e. sync files via `gsutil` or `rclone`.

![User Interace](interface.png)

Currently, you need to clone the repository and install the dependencies using [`poetry`](https://python-poetry.org/):

```
git clone https://bitbucket.org/scicomcore/bucket-pusher.git
cd gcs-uploader
poetry install
```

To support [end-user authentication](https://cloud.google.com/docs/authentication/end-user) via OAuth, you then need to [create a client-secrets file](https://cloud.google.com/bigquery/docs/authentication/end-user-installed#client-credentials) via the Google Cloud Platform for your project. If you would like to use service-account authentication, you will need to [create a service account key file](https://cloud.google.com/docs/authentication/production#create_service_account) and download it to your computer (here referred to as a 'service-account file'). After installing the package and downloading the relevant files, run

```
bucketpusher gen-key <key-file>
```

to generate an application encryption key file used to encrypt the relevant private fields of each data file (including user authentication credentials, if used). This is a rather thin layer of protection, since the key is bundled with the application, but arguably better than sending out raw credential files.

First create a `data` folder at the root of your project (if you have cloned this repository, just use the existing one). Next, encrypt and pickle the client-secrets file

```
bucketpusher pickle-client-secrets-file <path-to-json-file> <key-file>
```

and the service-account file

```
bucketpusher pickle-service-account-file <path-to-client-secrets-json-file> <key-file> --output-file <another-location> --bucket <default-bucket>
```

These commands will encrypt the relevant private fields of each JSON file obtained from GCP, and pickle the result to the `data` folder at the root of your project. For the last command, one can include the bucket ID in the pickle file, should you wish to load it automatically, rather than have the user specify it. Further, use the `--output-file` option to specify an alternate location, which is useful if you need to encrypt several service-account files.

You can then start the GUI using

```
bucketpusher gui <key-file> --with-authentication --with-service-account --with-bucket-id --title "GCS Uploader"
```

Remove each flag to see the effect.

# Freezing the application into a standalone

In practice, you will likely want to bundle the application into a standalone executable to distribute to end users. To do this, edit the entry-point file `app.py` as you necessary, but note that the application key you generated using `bucketpusher gen-key` is accessed via a Python module called `appykey`:

```
#appkey.py - don't version
appkey="my-key"  # 👈 Add your key here
```

This is so that `pyinstaller` can encrypt the key itself as part of the Python module byte-code obfuscation. This step is optional but recommend (you could simply specify the key as a string, or bundle it via `data`).

When instantiating `bucketpusher.App`, you can specify the location of the pickle files, log file, and user credentials, but I recommend using the default arguments.

On a macOS system, executing `make macos` from the root of your project will generate an application bundle in the `dist` directory. If you have Docker installed, run `make windows` to build the Windows executable. See `./macos_build.sh` and `./windows_build.sh` for details.
