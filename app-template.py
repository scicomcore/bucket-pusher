import datetime
from bucketpusher import app, config
from bucketpusher.__version__ import __version__
from loguru import logger

log_path = config.get_log_file_path()
logger.remove()
logger.enable("bucketpusher")
logger.add(
    log_path,
    level="INFO",
    backtrace=False,
    diagnose=False,
    rotation="50 MB",
)

app = app.App(
    key="",
    title=f"File Uploader - version {__version__}",
    with_authentication=False,
    with_service_account=True,
    with_bucket_id=False,
    prefix=lambda: datetime.datetime.now().strftime("%d-%m-%Y-%H:%M:%S"),
)
